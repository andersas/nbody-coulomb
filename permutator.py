#!/usr/bin/python

from __future__ import print_function
import nbody
import nrigidbody
import csvparse
import numpy
import multiprocessing
import copy
import time

e = 1.6021765699999999e-19;



def get_angle(particles,t,integrator):

     positions,velocities = integrator(particles,t)

     vf = velocities[:,0];
     vn = velocities[:,1];

     a = vf[-1];
     b = vn[-1];

     angle = numpy.arccos(a.dot(b)/(numpy.linalg.norm(a)*numpy.linalg.norm(b)))/numpy.pi*180

     return angle,particles;



particles = csvparse.parse_csv("test.csv")

num_particles = len(particles)

count = numpy.zeros((len(particles),));
for particle in particles:
     count[particle.c] += 1;

if (numpy.any(count==0)):
     integrator = nrigidbody.get_particle_state;
else:
     integrator = nbody.get_particle_state;
 


#t = numpy.arange(0,250e-15,1e-15)
#t = numpy.arange(0,10e-12,1e-15)
t = numpy.array([0,10e-12])


def update_charges():
     for i in range(len(charges)):
          particles[i].q = charges[i];

def submit_to_pool():
     update_charges();
     particles2 = copy.deepcopy(particles);
     pool_results.append(p.apply_async(get_angle,(particles2,t,integrator)));

def permutate(i):
     charges[i] = 0;
     for c in [0,e]:
          charges[i] = c;
          if (i == len(charges)-1):
               submit_to_pool();
          else:
               permutate(i+1);
     charges[i] = 0;

if (name == "__main__"):
     p = multiprocessing.Pool();
     pool_results = [];
     charges = [e,e,e,e,e,e,e,e,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

     permutate(8);

     results = [];

     for result in pool_results:
          angle,particles = result.get();
          charges = [int(round(particle.q/e,0)) for particle in particles];
          
          results.append(([angle],charges));

          for c in charges:
               print(str(c),end="");
          print(" " + str(angle));


     p.close();
     p.join();

     numpy.save("results.npy", numpy.array(results));


