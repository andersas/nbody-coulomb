#!/usr/bin/python

from __future__ import print_function
import periodictable # see https://pypi.python.org/pypi/periodictable
import scipy
import scipy.integrate
import numpy
import matplotlib.pyplot as plt

from particle import particle


def parse_csv(filename):

     # Parse CSV file with
     # name, fragment id, x, y, z, charge, vx,vy,vz.
     # The charge is in units of the elementary charge.

     dtypes = [('string','|S10'), numpy.int] + [numpy.double]*7;

     table = numpy.genfromtxt(filename,dtype=dtypes,comments="#");
      
     for row in table:
          row[5] *= 1.60217657e-19;

     u = 1.6605389e-27;

     particles = [];

     for row in table:
          mass = u*float(getattr(periodictable.elements,row[0]).mass)
          charge = row[5];
          fragment_id = row[1];
          x = numpy.array([row[2], row[3], row[4]]);
          v = numpy.array([row[6], row[7], row[8]]);
          p = particle(mass,charge,fragment_id,x,v);
          particles.append(p);

     return particles


