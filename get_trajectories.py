#!/usr/bin/python

from __future__ import print_function
import nbody
import csvparse
import numpy

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D


from particle import particle

import nbody
import nrigidbody



particles = csvparse.parse_csv("test.csv")

t = numpy.arange(0,1.0e-12,1e-15)
#t = numpy.array([0,1e-12])

count = numpy.zeros((len(particles),));

for particle in particles:
     count[particle.c] += 1;

if (numpy.any(count==0)):
     positions,velocities = nrigidbody.get_particle_state(particles,t)
else:
     positions,velocities = nbody.get_particle_state(particles,t)

vf = velocities[:,0];
vn = velocities[:,1];

angles = [numpy.arccos(a.dot(b)/(numpy.linalg.norm(a)*numpy.linalg.norm(b)))/numpy.pi*180 for a,b in zip(vf,vn)]

angles = numpy.array(angles);

numpy.save("time.npy",t);
numpy.save("positions.npy",positions);
numpy.save("velocities.npy",velocities);
numpy.save("angles.npy",angles);

