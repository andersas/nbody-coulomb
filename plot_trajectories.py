#!/usr/bin/python

from __future__ import print_function
import nbody
import csvparse
import numpy

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D


from particle import particle

import nbody
import nrigidbody


t = numpy.load("time.npy");
positions = numpy.load("positions.npy");
velocities = numpy.load("velocities.npy");
angles = numpy.load("angles.npy");

num_particles = positions[:,0].shape


colors = cm.rainbow(numpy.linspace(0, 1, positions.shape[1]))

#print(positions.shape)
for i in range(positions.shape[1]):
     plt.plot(positions[:,i,0],positions[:,i,1],color=colors[i],label=str(i),linestyle="solid");

plt.xlim([-1e-9,1e-9])
plt.ylim([-1e-9,1e-9])
plt.legend()

#plt.figure()
#plt.plot(trajectories[1][:,0]);

x = positions[:,0];
y = positions[:,1];

vf = velocities[:,0];
vn = velocities[:,1];

angles = [numpy.arccos(a.dot(b)/(numpy.linalg.norm(a)*numpy.linalg.norm(b)))/numpy.pi*180 for a,b in zip(vf,vn)]
#angles2d = [numpy.arccos(a.dot(b)/(numpy.linalg.norm(a)*numpy.linalg.norm(b)))/numpy.pi*180 for a,b in zip(vf,vn)]


#theta = numpy.arctan2(y,x)*180/numpy.pi;

plt.figure()
plt.plot(t,angles);

for i in range(len(t)):
     print (t[i],angles[i]);

#print("Angle: " + str(theta))

f = plt.figure()
ax = f.add_subplot(111, projection='3d')
for i in range(positions.shape[1]):
     ax.plot(positions[:,i,0],positions[:,i,1],zs=positions[:,i,2],color=colors[i],label=str(i),linestyle="solid");

ax.set_xlabel("x")
ax.set_ylabel("y");
ax.set_zlabel("z");
ax.set_xlim([-1e-9,1e-9])
ax.set_ylim([-1e-9,1e-9])
ax.set_zlim([-1e-9,1e-9])

print(numpy.linalg.norm(vf[-1]), numpy.linalg.norm(vn[-1]));


plt.show();

