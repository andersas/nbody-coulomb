#!/usr/bin/python

import argparse
import sys

class atom(object):
     
     def __init__(s,name,x,y,z):
          s.name = name
          s.x = x
          s.y = y
          s.z = z

parser = argparse.ArgumentParser(description="Dump all positions and weights of molecules from a .mol file to a .csv file.");
parser.add_argument('molfile',nargs="?",type=str,help="Name of file. If not given, read from stdin.");


args = parser.parse_args();

filename = args.molfile;

if (filename != None):
     f = open(filename,"r");
else:
     f = sys.stdin;

atoms = [];

in_atoms = False;
for line in f:
     if line.find("BEGIN ATOM") != -1:
          in_atoms = True;
          continue;

     if line.find("END ATOM") != -1:
          break;
     
     if (in_atoms):
          cont = line.split(" ");
          atoms.append(atom(cont[4],cont[5],cont[6],cont[7]));

i = 0;
for atom in atoms:
     print(atom.name + "\t" + str(i) + "\t" + atom.x + "\t" + atom.y + "\t" + atom.z);
     i += 1;




