#!/usr/bin/python

from __future__ import print_function
import numpy
from numpy import cos, sin

angle = 40/180.0*numpy.pi;

rotation_matrix = numpy.array([[cos(angle),0,sin(angle)],[0,1,0],[-sin(angle),0,cos(angle)]]);


coords = numpy.array([[-2.2,-2.8,0],
                     [-1.2,-3.4,0],
                     [-1.2,-4.8,0],
                     [-2.9,-5.8,0],
                     [2.2,-2.8,0],
                     [1.2,-3.4,0],
                     [1.2,-4.8,0],
                     [2.9,-5.8,0]]).transpose();

print(coords);
print();
print(rotation_matrix.dot(coords));

print(rotation_matrix.dot(coords).shape);
