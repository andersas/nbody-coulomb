#!/usr/bin/python3

from __future__ import print_function
import scipy
import scipy.integrate
import numpy
import numpy.linalg
import matplotlib.pyplot as plt
from particle import particle

k = 8.9875517873681764e9; # Coulombs constant

def unpack(y,N):
  
     x = y[0:3*N].reshape((N,3));
     v = y[3*N:2*3*N].reshape((N,3));

     return (v,x);

def pack(v,x):

     return numpy.concatenate((x,v)).flatten()


def calculate_forces(x,q):
     """ Calculate force on each particle """

     F = numpy.zeros_like(x);
     for i in range(len(x[:,0])):
          for j in range(len(x[:,0])):
               if (i == j):
                    continue;
               distance = numpy.linalg.norm(x[i]-x[j]);
               F[i] += q[i]*q[j]/distance**3*(x[i]-x[j])

     F *= k;

     return F;


def derivative(y,t,N,m,q):
     """ 
     y: State vector
     t: time
     N: Number of particles
     m: Masses of particles
     q: Charges of particles

     Returns the differential to the state vector.

     """
     #print(t);
     v,x = unpack(y,N);
     #dimension = len(v[0]);

     F = calculate_forces(x,q);

     dx_dt = numpy.zeros_like(x);
     dv_dt = numpy.zeros_like(x);
     for i in range(N):
          dx_dt[i,:] = v[i];
          dv_dt[i,:] = F[i]/m[i];

     return pack(dv_dt,dx_dt);


def get_particle_state(particles,t,atol=1.0e-8,rtol=1.5e-8):
     
     N = len(particles);
     
     x = numpy.zeros((N,3));
     v = numpy.zeros((N,3));
     
     m = numpy.zeros((N,));
     q = numpy.zeros((N,));

     for i in range(N):
          x[i,:] = particles[i].x;
          v[i,:] = particles[i].v;

          m[i] = particles[i].m;
          q[i] = particles[i].q;

     y0 = pack(v,x);

     #result = scipy.integrate.odeint(derivative,y0,t,args=(N,m,q,c,I),rtol=rtol,atol=atol);
     result = scipy.integrate.odeint(derivative,y0,t,args=(N,m,q),h0=1e-17,hmin=1e-18,atol=1e-1,rtol=1e-1);
     
     positions = [];
     velocities = [];

     for i in range(result.shape[0]):
          v,x = unpack(result[i],N);
          positions.append(x);
          velocities.append(v);

     return numpy.array(positions),numpy.array(velocities);
