#!/usr/bin/python3

from __future__ import print_function
import scipy
import scipy.integrate
import numpy
import numpy.linalg
import matplotlib.pyplot as plt
from particle import particle

k = 8.9875517873681764e9; # Coulombs constant


def pack(X,Q,P,L):

     X = X.flatten();
     Q = Q.flatten();
     P = P.flatten();
     L = L.flatten();

     return numpy.concatenate((X,Q,P,L));

def unpack(state,N):

     start = 0;
     end = start + 3*N;
     X = state[start:end].reshape((N,3));

     start = end;
     end = start + 4*N;
     Q = state[start:end].reshape((N,4));

     start = end;
     end = start + 3*N;
     P = state[start:end].reshape((N,3));
     
     start = end;
     end = start + 3*N;
     L = state[start:end].reshape((N,3));

     return (X,Q,P,L)

def calculate_mass_centres(x,m,c):
     num_fragments = numpy.max(c) + 1;
     centres = numpy.zeros((num_fragments,3));
     masses = numpy.zeros((num_fragments,));

     for i in range(len(x[:,0])):
          centres[c[i],:] += m[i]*x[i];
          masses[c[i]] += m[i];
     
     for i in range(num_fragments):
          centres[i] /= masses[i];

     return centres,masses;




def normalize(q):
     return q/numpy.linalg.norm(q);

def QuaterionToMatrix(q):
     
     w,x,y,z = q[0],q[1],q[2],q[3];

     wx = 2*w*x
     wy = 2*w*y;
     wz = 2*w*z;
     xx = 2*x*x;
     xy = 2*x*y;
     xz = 2*x*z;
     yy = 2*y*y;
     yz = 2*y*z;
     zz = 2*z*z;

     return numpy.array([
          [1 - yy - zz, xy - wz, xz + wy],
          [xy + wz, 1 - xx - zz, yz - wx],
          [xz - wy, yz - wx, 1 - xx - yy]
          ]);
     
def HamiltonProduct(a,b):

     r1 = a[0];
     r2 = b[0];

     v1 = a[1:];
     v2 = b[1:];

     r = r1*r2 - v1.dot(v2)
     v = r1*v2 + r2*v1 + numpy.cross(v1,v2);

     return numpy.concatenate((numpy.array([r]),v));

# To speed things up, maybe pass R.dot(body_positions)
# instead of R and body_positions...
def get_particle_velocities(R,V,omega,body_positions,c):

     world_velocities = numpy.empty(body_positions.shape);
     CM_velocity = V;

     for i in range(len(body_positions[:,0])):
          r = R[c[i]].dot(body_positions[i])
          CM_relative_velocity = numpy.cross(omega[c[i]],r);
          world_velocities[i,:] = CM_velocity[c[i],:] + CM_relative_velocity;

     return world_velocities;

def body_to_world(X,R,body_positions,c):

     world_positions = numpy.empty(body_positions.shape);

     for i in range(len(body_positions[:,0])):
          world_positions[i,:] = R[c[i]].dot(body_positions[i]) + X[c[i]]

     return world_positions;

def world_to_body(X,R,world_positions,c):

     body_positions = numpy.empty(world_positions.shape);

     for i in range(len(world_positions[:,0])):
          body_positions[i,:] = R[c[i]].transpose().dot((world_positions[i] - X[c[i]]))

     return body_positions;


def calculate_force_torque(X,R,particle_body_positions,particle_charges,c):

     particle_positions = body_to_world(X,R,particle_body_positions,c);

     F = numpy.zeros((numpy.max(c)+1,3));
     T = numpy.zeros_like(F);
     
     num_particles = len(particle_positions[:,0]);
     for i in range(num_particles):
          
          f = numpy.zeros((3,));
          for j in range(num_particles):
               if (i == j):
                    continue;
               r = particle_positions[i]-particle_positions[j];
               distance = numpy.linalg.norm(r);
               f += particle_charges[i]*particle_charges[j]/distance**3*r

          F[c[i]] += f;
          T[c[i]] += numpy.cross(particle_positions[i]-X[c[i]], f);

     return k*F,k*T


def get_auxiliary_state(X,Q,P,L,Iinvbody,mass):

     N = len(X[:,0]);

     V = numpy.empty((N,3));
     R = numpy.empty((N,3,3));
     Iinv = numpy.empty((N,3,3));
     omega = numpy.empty((N,3));
     for i in range(N):
          V[i,:] = P[i,:]/mass[i]
          R[i,:] = QuaterionToMatrix(normalize(Q[i])) 
          Iinv[i,:] = R[i].dot(Iinvbody[i]).dot(R[i].transpose())
          omega[i,:] = Iinv[i].dot(L[i]); 

     return V,R,Iinv,omega


def derivative(state,time,mass,particle_body_positions,particle_charges,c,Iinvbody):
     #print(t);

     N = numpy.max(c)+1

     X,Q,P,L = unpack(state,N);
    
     V,R,Iinv,omega = get_auxiliary_state(X,Q,P,L,Iinvbody,mass);

     Qdot = numpy.empty((N,4));
     for i in range(N):
          Qdot[i,:] = 0.5*HamiltonProduct(numpy.concatenate((numpy.array([0]),omega[i])),Q[i]);

     F,T = calculate_force_torque(X,R,particle_body_positions,particle_charges,c)
     #print(time,L[1],omega[1]);
     #print(time,V[1],Qdot[1],F[1],T[1])
     #print(Iinv[1]);
     #print(time,P[0],mass[0])

     return pack(V,Qdot,F,T);


def get_particle_state(particles,t,atol=1.0e-8,rtol=1.5e-8):
     
     N = len(particles);

     x = numpy.zeros((N,3));
     m = numpy.zeros((N,));
     q = numpy.zeros((N,));
     c = numpy.zeros((N,),dtype=numpy.int);

     for i in range(N):
          x[i,:] = particles[i].x;
          m[i] = particles[i].m;
          q[i] = particles[i].q;
          c[i] = particles[i].c;

     N_bodies = numpy.max(c) + 1

     mass_centres,masses = calculate_mass_centres(x,m,c);

     R = numpy.array([numpy.identity(3)]*N_bodies);

     body_positions = world_to_body(mass_centres,R,x,c);

     Ibody = numpy.zeros((N_bodies,3,3));
     Q = numpy.empty((N_bodies,4));
     P = numpy.zeros((N_bodies,3));
     L = numpy.zeros((N_bodies,3));
     xs = x;

     for i in range(N):
          r = body_positions[i];
          x,y,z = r[0],r[1],r[2]
          Ixx = m[i]*(y**2+z**2);
          Iyy = m[i]*(x**2+z**2);
          Izz = m[i]*(x**2+y**2);
          Ixy = -m[i]*x*y;
          Ixz = -m[i]*x*z;
          Iyz = -m[i]*y*z;

          Ibody[c[i]] += numpy.array([
               [Ixx, Ixy, Ixz],
               [Ixy, Iyy, Iyz],
               [Ixz, Iyz, Izz]
               ]);
     
          Q[c[i],:] = numpy.array([1,0,0,0]); # No rotation
          
          # Don't change P (initially no (body) movement)
          # Don't change L (initially no rotation)

     x = xs;

     Iinvbody = numpy.empty(Ibody.shape);
     for i in range(len(Iinvbody[:,0])):
          Iinvbody[i,:] = numpy.linalg.pinv(Ibody[i]);

     y0 = pack(mass_centres,Q,P,L);

     #result = scipy.integrate.odeint(derivative,y0,t,args=(N,m,q,c,I),rtol=rtol,atol=atol);
#def derivative(state,time,mass,particle_body_positions,particle_charges,c,Iinvbody):
     #result = scipy.integrate.odeint(derivative,y0,t,args=(masses,body_positions,q,c,Iinvbody),h0=1e-17,hmin=1e-18,atol=1e-1,rtol=1e-1);
     result = scipy.integrate.odeint(derivative,y0,t,args=(masses,body_positions,q,c,Iinvbody),h0=1e-17,hmin=1e-18);
     
     positions = []
     velocities = [];

     for i in range(result.shape[0]):
          X,Q,P,L = unpack(result[i],N_bodies);
          V,R,Iinv,omega = get_auxiliary_state(X,Q,P,L,Iinvbody,masses);
          x = body_to_world(X,R,body_positions,c);
          v = get_particle_velocities(R,V,omega,body_positions,c);
          positions.append(x);
          velocities.append(v);

     return numpy.array(positions), numpy.array(velocities);
